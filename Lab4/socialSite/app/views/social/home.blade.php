@extends('layouts.master')

@section('title')
Social Site
@stop

@section('content')
     <div class='row'>
          <div class='col-sm-3'>
              <form>
                  Name: <br>
                  <input type="text" name="Name"/> <br>
                  Message: <br>
                  <textarea rows='4' cols='23'>Enter your Post</textarea><br>
                  <input type="submit" value="Submit"/>
              </form>
          </div>
          <div class='col-sm-9'>
            <!-- random number generater & post maker-->
            <?php
              for ($i = 0; $i < $num; $i++){
                $image = $posts[$i]['image'];
                $date = $posts[$i]['date'];
                $message = $posts[$i]['message'];
                echo "<div class='post'>";
                echo "<img class='photo'src='$image' alt ='profile pic'>";
                echo "Date: ";
                echo $date;
                echo "<br>";
                echo "Message: ";
                echo $message;
                echo "<br>";
                echo "</div>";
              }
            ?>
             <a href={{ secure_url('/friends') }}>Friends</a>
          </div>
      </div> 
@stop