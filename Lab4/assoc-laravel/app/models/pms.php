<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getPms()
{
  $pms = array(
      array( 'name' => 'Harold Holt', 'address' => '4 john street', 'email' => 'har@gmail.com', 'phone' => '092318923'),
      array( 'name' => 'John McEwen', 'address' => '5 john street', 'email' => 'jo@gmail.com', 'phone' => '024781442'),
      array( 'name' => 'John Gorton', 'address' => '6 john street', 'email' => 'joh@gmail.com', 'phone' => '8346217433'),
      array( 'name' => 'William McMahon', 'address' => '7 john street', 'email' => 'wil@gmail.com', 'phone' => '7491263495'),
      array( 'name' => 'Gough Whitlam', 'address' => '8 john street', 'email' => 'gog@gmail.com', 'phone' => '836549264'),
      array( 'name' => 'Malcolm Fraser', 'address' => '9 john street', 'email' => 'mal@gmail.com', 'phone' => '292754934'),
      array( 'name' => 'Bob Hawke',  'address' => '10 john street', 'email' => 'bob@gmail.com', 'phone' => '89127423'),
      array( 'name' => 'Paul Keating', 'address' => '11 john street', 'email' => 'pau@gmail.com', 'phone' => '124234234'),
      array( 'name' => 'John Howard', 'address' => '12 john street', 'email' => 'john@gmail.com', 'phone' => '3656245456'),
      array( 'name' => 'Kevin Rudd', 'address' => '13 john street', 'email' => 'kev@gmail.com', 'phone' => '435345456'),
      array( 'name' => 'Julia Gillard', 'address' => '14 john street', 'email' => 'jul@gmail.com', 'phone' => '35345346') 
  );
  return $pms;
}
?>

