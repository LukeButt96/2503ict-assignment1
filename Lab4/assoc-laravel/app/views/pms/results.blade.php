@extends('layouts.master')

@section('title')
Associative array search results page
@stop

@section('content')

<h2>Australian Prime Ministers</h2>
<h3>Results</h3>

@if (count($pms) == 0)

<p>No results found.</p>

@else 
<?php
  $query = Input::get('query');
  echo "<p>Results for '$query'</p>"
?>
<table class="bordered">
<thead>
<th>Name</th><th>Email</th><th>Phone</th><th>Address</th>
</thead>
<tbody>

@foreach($pms as $pm)
  <tr><td>{{{ $pm['index'] }}}</td><td>{{{ $pm['name'] }}}</td><td>{{{ $pm['email'] }}}</td><td>{{{ $pm['phone'] }}}</td><td>{{{ $pm['address'] }}}</td></tr>
@endforeach

</tbody>
</table>
@endif

<p><a href="{{ secure_url('/') }}">New search</a></p>
@stop