<!-- Luke Butt SID:2943747 07/03/2015 -->
<?php
  $num = rand(1,10);
  $posts = array(
    array('date' => '27 Jan 2014', 'message'=> 'hello0', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello1', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello2', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello3', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello4', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello5', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello6', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello7', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello8', 'image' => 'images/dogProfilePic.jpeg'),
    array('date' => '27 Jan 2014', 'message'=> 'hello9', 'image' => 'images/dogProfilePic.jpeg'),
    );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Luke Butt">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
    
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network <?php echo $num; ?></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Photos </a></li>
              <li><a href="../navbar-static-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main Body -->
      
      <div class='row'>
          <div class='col-sm-3'>
              <form>
                  Name: <br>
                  <input type="text" name="Name"/> <br>
                  Message: <br>
                  <textarea rows='4' cols='23'>Enter your Post</textarea><br>
                  <input type="submit" value="Submit"/>
              </form>
          </div>
          <div class='col-sm-9'>
            <!-- random number generater & post maker-->
            <?php
              for ($i = 0; $i < $num; $i++){
                $image = $posts[$i]['image'];
                $date = $posts[$i]['date'];
                $message = $posts[$i]['message'];
                echo "<div class='post'>";
                echo "<img class='photo'src='$image' alt ='profile pic'>";
                echo "Date: ";
                echo $date;
                echo "<br>";
                echo "Message: ";
                echo $message;
                echo "<br>";
                echo "</div>";
              }
            ?>
          </div>
      </div> 
      
    </div> <!-- /container -->
    
  </body>
</html>
